SOURCES = $(realpath sources.txt)

all: html txt

html: prep
	makeinfo --html --no-split build/start.texi -o build/out/index.html

txt: prep
	makeinfo --plaintext build/start.texi -o build/out

prep:
	mkdir -p build/out
	perl setup.pl $(SOURCES) build

.PHONY: clean

clean:
	rm -rf build/*
