#!/usr/bin/perl

use strict;
use warnings;

use File::Temp qw/ tempdir /;
use File::Copy qw/ copy /;
use File::Path qw/ make_path /;
use File::Spec::Functions qw / catfile /;
use File::Basename qw/ dirname /;
use List::Util qw/ uniq /;
use Cwd qw/ abs_path /;

my $wd = dirname($0);

my $sources = catfile($wd, 'sources.txt');
if ($#ARGV > -1) {
	$sources = abs_path($ARGV[0]);
}
print STDERR "using source $sources\n";

my $d;
if ($#ARGV > 0) {
	$d = abs_path($ARGV[1]);
} else {
	$d = tempdir( CLEANUP => 0);
}
print STDERR "using path $d\n";

my @base_files = (
	'start.texi',
	'overview.texi',
);

foreach my $fn (@base_files) {
	my $src = catfile($wd, $fn);
	my $dst = catfile($d, $fn);
	copy($src, $dst);
}

my $d_ext = catfile($d, 'ext');
make_path($d_ext);

my @git_src_list;
my %src_list;
open(my $fh, '<', $sources);
while (<$fh>) {
	my $l = $_;
	chomp($l);
	
	my ($url, $path) = split(/\t/, $l);
	my @parts = split(/\//, $url);
	$parts[$#parts] =~ /^([^\.]+).*$/;

	push(@git_src_list, $url);
	my $src_item = catfile($d, 'ext', $1, $path);
	$src_list{$1} = $src_item;
}
close($fh);

foreach my $url (uniq(@git_src_list)) {
	my @parts = split(/\//, $url);
	$parts[$#parts] =~ /^([^\.]+).*$/;

	my $dst = catfile($d_ext, $1);
	system('git', 'clone', $url, $dst);
}
close($fh);

my $fn_index = catfile($d, 'start.texi');
my $fn_appendix = catfile($d, 'appendix.texi');
unlink($fn_appendix);

open(my $fh_out, '>>', $fn_index);
open(my $fh_out_appendix, '>>', $fn_appendix);

print $fh_out qq{\@menu
* top-overview ::
};

my @includes;

foreach my $k (keys %src_list) {
	my $dn_texi = $src_list{$k};
	print STDERR "dn texi " . $dn_texi . " " . $k . "\n";
	my $part_path = catfile($d, $k . '.texi');
	my $fh_texi_in;
	my $fh_texi_out;
	open ($fh_texi_out, '>', $part_path);
	print $fh_texi_out qq{\@node $k
\@chapter $k

};

	my $in_path = catfile($dn_texi, 'content.texi');
	open ($fh_texi_in, '<', $in_path);
	while (<$fh_texi_in>) {
		next unless substr($_, 0, 8) eq '@include';
		$_ =~ /^\@include\s+(.+)$/;
		print $fh_texi_out qq{\@include $dn_texi/$1
};
	}
	close($fh_texi_in);
	close($fh_texi_out);

	push(@includes, $part_path);
	#print $fh_out qq{\@include $part_path
	print $fh_out qq{* $k ::
};

	$in_path = catfile($dn_texi, 'appendix.texi');
	open ($fh_texi_in, '<', $in_path) or next;
	while (<$fh_texi_in>) {
		next unless substr($_, 0, 8) eq '@include';
		$_ =~ /^\@include\s+(.+)$/;
		print $fh_out_appendix qq{\@include $dn_texi/$1
};
	}
	close($fh_texi_in);
}

close($fh_out_appendix);

print $fh_out qq{\@end menu

\@include overview.texi
};

foreach my $include (@includes) {
	print $fh_out qq{\@include $include
};
}

print $fh_out qq{\@include appendix.texi
};

close($fh_out);
