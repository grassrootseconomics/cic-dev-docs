FROM debian:jessie

WORKDIR root

RUN apt-get update -y
RUN apt-get install perl make gcc git texinfo webfs -y
RUN cpan List::Util

COPY setup.pl Makefile start.texi overview.texi sources.txt cpanfile ./
COPY mime.types /etc/

RUN make clean
RUN make html

RUN useradd -d /srv/http http
RUN mkdir -vp /srv/http && \
	mv /root/build/out/* /srv/http/ && \
	chown http:http /srv/http/ -R && \
	chmod 755 /srv/http -R

USER http
WORKDIR /srv/http

ENTRYPOINT [ "/usr/bin/webfsd", "-4", "-d", "-F", "-p", "8080", "-r", "/srv/http", "-f", "index.html" ]
